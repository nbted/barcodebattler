package com.example.deptinfo.barcodebattler;

/**
 * Created by hp2015 on 27/10/2017.
 */

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by hp2015 on 25/10/2017.
 */
public class DtabaseHandler extends SQLiteOpenHelper {


    private static final String DATABASE_NAME="BATTLER_DB",
            NOM_TABLE="BATTLER",
            KEY_ID="id",
            NOM_MONSTER="nom",
            POINT_VIE="pointDeVie",
            DEGAT="degat",
            ARMUR="armur",
            KEY_IMAGE="drawbaleImg",
            HASH="hash";
    private static final String Creat_bdd= "CREATE TABLE "+NOM_TABLE+" ("+KEY_ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+NOM_MONSTER+" TEXT NOT NULL, "+POINT_VIE+" INTEGER NOT NULL, "+DEGAT+" INTEGER NOT NULL, "+ARMUR+" INTEGER NOT NULL, "+ KEY_IMAGE + " INTEGER, "+ HASH +" TEXT NOT NULL );";
    public DtabaseHandler(Context context, String name,
                             SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Creat_bdd);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String dropdb="DROP TABLE "+NOM_TABLE+";";
        db.execSQL(dropdb);
        onCreate(db);
    }

}