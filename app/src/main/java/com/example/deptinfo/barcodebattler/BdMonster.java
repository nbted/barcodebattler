package com.example.deptinfo.barcodebattler;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by khugu on 27/10/2017.
 */

public class BdMonster  {

    private static final int DATABASE_VERSION = 2;
    private static final String DATABASE_NAME="BATTLER_DB",
            NOM_TABLE="BATTLER",
            KEY_ID="id",
            NOM_MONSTER="nom",
            POINT_VIE="pointDeVie",
            DEGAT="degat",
            ARMUR="armur",
            KEY_IMAGE="drawbaleImg",
            HASH="hash";

    private SQLiteDatabase bdd;
    private DtabaseHandler dbh;


    public BdMonster(Context context) {
        dbh=new DtabaseHandler(context, DATABASE_NAME, null, DATABASE_VERSION);

    }

    public void open(){

        bdd=dbh.getWritableDatabase();
    }
    public void close(){
        bdd.close();
    }
    public SQLiteDatabase getBDD(){
        return bdd;
    }


    public  long addMonster(Monster monster){

        ContentValues values = new ContentValues();

        values.put(NOM_MONSTER,monster.getNom());
        values.put(POINT_VIE,monster.getPointDeVie());
        values.put(DEGAT,monster.getDegat());
        values.put(ARMUR,monster.getArmur());
        values.put(KEY_IMAGE,monster.getDrawbaleImg());
        values.put(HASH,monster.getHash());

        return bdd.insert(NOM_TABLE,null,values);


    }

    public  int updateMonster(int id,Monster monster){

        ContentValues values = new ContentValues();

        values.put(NOM_MONSTER,monster.getNom());
        values.put(POINT_VIE,monster.getPointDeVie());
        values.put(DEGAT,monster.getDegat());
        values.put(ARMUR,monster.getArmur());
        values.put(KEY_IMAGE,monster.getDrawbaleImg());
        values.put(HASH,monster.getHash());
        int rowsAffected = bdd.update(NOM_TABLE, values, KEY_ID + "="+id,null);

        return rowsAffected;
    }
    public int removeMonsterWithId(int id){
        return bdd.delete(NOM_TABLE,KEY_ID+"="+id,null);
    }

    private Monster cursorToMonster(Cursor c){
        if(c.getCount()==0)
            return null;
        c.moveToFirst();
        Monster ms=new Monster();
        ms.setId(c.getInt(0));
        ms.setNom(c.getString(1));
        ms.setPointDeVie(c.getInt(2));
        ms.setDegat(c.getInt(3));
        ms.setArmur(c.getInt(4));
        ms.setDrawbaleImg(c.getInt(5));
        ms.setHash(c.getString(6));
        c.close();
        return ms;

    }
    private List<Monster>  cursorToMonsters(Cursor c){
        if(c.getCount()==0)
            return new ArrayList<Monster>(0);
        List<Monster> retMonster=new ArrayList<Monster>(c.getCount());
        c.moveToFirst();
        do{
            Monster ms=new Monster();
            ms.setId(c.getInt(0));
            ms.setNom(c.getString(1));
            ms.setPointDeVie(c.getInt(2));
            ms.setDegat(c.getInt(3));
            ms.setArmur(c.getInt(4));
            ms.setDrawbaleImg(c.getInt(5));
            ms.setHash(c.getString(6));
            retMonster.add(ms);
        }while(c.moveToNext());
        c.close();
        return retMonster;
    }

    public List<Monster> getAllMonsters() {
        Cursor c=bdd.query(NOM_TABLE,new String[]{ KEY_ID, NOM_MONSTER, POINT_VIE, DEGAT, ARMUR, KEY_IMAGE, HASH},null,null,null,null,null);
        return cursorToMonsters(c);
    }


}
