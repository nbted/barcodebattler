package com.example.deptinfo.barcodebattler;

/**
 * Created by deptinfo on 31/10/2017.
 */
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.example.deptinfo.barcodebattler.AcceptThread;
import com.example.deptinfo.barcodebattler.MyBluetoothService;

import java.io.IOException;
import java.util.UUID;



class ConnectThread extends Thread {
    public static final UUID MY_UUID=new UUID(2,5);
    //public static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    protected static final int SUCCESS_CONNECT = 0;
    protected static final int MESSAGE_READ = 1;
    protected static final int MESSAGE_WRITE = 2;
    String tag="bugging";
    BluetoothAdapter btAdapter;

    private final BluetoothSocket mmSocket;
    private final BluetoothDevice mmDevice;
    Handler mHandler = new Handler(){
        @Override
        public void handleMessage(Message msg)
        {
            // TODO Auto-generated method stub
            Log.i(tag, "in handler");
            super.handleMessage(msg);
            switch(msg.what)
            {
                case SUCCESS_CONNECT:
                    // DO something
                    MyBluetoothService mbs = new MyBluetoothService(mmSocket,this);

                    //   Toast.makeText(getApplicationContext(), "CONNECT", 0).show();

                    String s = "successfully connected";
                    mbs.cthread.write(s.getBytes());
                    Log.i(tag, "connected");
                    break;
                case MESSAGE_READ:
                    byte[] readBuf = (byte[])msg.obj;
                    String string = new String(readBuf);
                     // Toast.makeText(getApplicationContext(), string, 0).show();
                    break;
                case MESSAGE_WRITE:
                    byte[] writeBuf = (byte[]) msg.obj;
                    // construct a string from the buffer
                    String writeMessage = new String(writeBuf);
                    break;
            }
        }
    };

    public ConnectThread(BluetoothDevice device) {
        // Use a temporary object that is later assigned to mmSocket,
        // because mmSocket is final
        BluetoothSocket tmp = null;
        mmDevice = device;
        Log.i(tag, "construct");
        // Get a BluetoothSocket to connect with the given BluetoothDevice
        try {
            // MY_UUID is the app's UUID string, also used by the server code
            tmp = device.createRfcommSocketToServiceRecord(MY_UUID);
        } catch (IOException e) {
            Log.i(tag, "get socket failed");

        }
        mmSocket = tmp;
    }

    public void run() {
        // Cancel discovery because it will slow down the connection
        //btAdapter.cancelDiscovery();
        Log.i(tag, "connect - run");
        try {
            // Connect the device through the socket. This will block
            // until it succeeds or throws an exception
           mmSocket.connect();
            Intent intent =new Intent();
            Monster m1 = (Monster)intent.getParcelableExtra("MONSTERADVERSE");
           // mbs.cthread.write(m1.getNom().getBytes());

            Log.i(tag, "connect - succeeded");
        } catch (IOException connectException) {    Log.i(tag, "connect failed");
            // Unable to connect; close the socket and get out
            try {
                mmSocket.close();
            } catch (IOException closeException) { }
            return;
        }

        // Do work to manage the connection (in a separate thread)

        //manageMyConnectedSocket(mmSocket);
    }



    /** Will cancel an in-progress connection, and close the socket */
    public void cancel() {
        try {
            mmSocket.close();
        } catch (IOException e) { }
    }
}