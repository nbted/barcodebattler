package com.example.deptinfo.barcodebattler;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class SelectMonster extends AppCompatActivity{
    public static final String PARAM_MONSTER = "PARAM_MONSTER";

    ListView monsters;
    BdMonster bdMonster;
    Monster ms;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_monsters);

         bdMonster=new BdMonster(this);
         bdMonster.open();
         monsters=(ListView)findViewById(R.id.listMonster);
        List<Monster> listMonster=new ArrayList<>();
        listMonster=bdMonster.getAllMonsters();
        SelectMonsterAdapter monAdapter=new SelectMonsterAdapter(this,listMonster);
        monsters.setAdapter(monAdapter);
        bdMonster.close();
        final List<Monster> finalListMonster = listMonster;
        monsters.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                ms=(Monster) monsters.getItemAtPosition(i);
               Intent intent=new Intent();
                intent.putExtra(PARAM_MONSTER, ms);
               setResult(RESULT_OK,intent);
               finish();
            }
        });



    }


}
