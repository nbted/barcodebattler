package com.example.deptinfo.barcodebattler;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.DataSetObserver;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class AllMonsters extends AppCompatActivity{

    ListView monsters;
    BdMonster bdMonster;
    Monster ms;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_monsters);
        bdMonster=new BdMonster(this);
        bdMonster.open();
        monsters=(ListView)findViewById(R.id.listMonster);
        List<Monster> listMonster=new ArrayList<>();
        listMonster=bdMonster.getAllMonsters();
        MonsterAdapter monAdapter=new MonsterAdapter(this,listMonster);
        monsters.setAdapter(monAdapter);
        bdMonster.close();
        monsters.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                ms=(Monster) monsters.getItemAtPosition(i);
                AlertDialog.Builder adb= new AlertDialog.Builder(AllMonsters.this);
                adb.setTitle("Monster Selectioner");
                adb.setMessage("Monster Info "+ms.getNom()+" "+ms.getId());
                adb.setPositiveButton("OK", null);
                adb.setNegativeButton("Annuler",null);
                adb.show();
            }
        });


    }


}
