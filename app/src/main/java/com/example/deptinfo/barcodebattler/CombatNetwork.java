package com.example.deptinfo.barcodebattler;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import static com.example.deptinfo.barcodebattler.SelectMonster.PARAM_MONSTER;

public class CombatNetwork extends AppCompatActivity {
    ImageView img,img1;
    Monster ms1,ms2;
    TextView txt1,txt2,txt3,txt4,deg,arm,deg1,arm1;
    int i=0;
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode==2 && resultCode == RESULT_OK){
            ms1=(Monster) data.getParcelableExtra(PARAM_MONSTER);
            img=(ImageView) findViewById(R.id.img);
            img.setImageResource(ms1.getDrawbaleImg());
            txt1=(TextView) findViewById(R.id.nom);
            txt2=(TextView) findViewById(R.id.pdv);
            deg=(TextView) findViewById(R.id.degat);
            arm=(TextView) findViewById(R.id.armur);
            txt1.setText("Nom : "+ms1.getNom());
            txt2.setText("VIE : "+ms1.getPointDeVie());
            deg.setText("DEGAT : "+ms1.getDegat());
            arm.setText("ARMUR : "+ms1.getArmur());
            i++;
            if(i==2){
                Button btn= (Button) findViewById(R.id.cmb);
                btn.setVisibility(View.VISIBLE);

            }
        }
        else if(requestCode==1 && resultCode == RESULT_OK){

            ms2=(Monster) data.getParcelableExtra(PARAM_MONSTER);
            img1=(ImageView) findViewById(R.id.imgadv);
            img1.setImageResource(ms2.getDrawbaleImg());
            txt3=(TextView) findViewById(R.id.nomadv);
            txt4=(TextView) findViewById(R.id.pdvadv);
            deg1=(TextView) findViewById(R.id.degatadv);
            arm1=(TextView) findViewById(R.id.armuradv);
            txt3.setText("Nom : "+ms2.getNom());
            txt4.setText("VIE : "+ms2.getPointDeVie());
            deg1.setText("DEGAT : "+ms2.getDegat());
            arm1.setText("ARMUR : "+ms2.getArmur());
            i++;
            if(i==2){
                Button btn= (Button) findViewById(R.id.cmb);
                btn.setVisibility(View.VISIBLE);

            }
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_combat_local);
        img=(ImageView) findViewById(R.id.img);
        img1=(ImageView) findViewById(R.id.imgadv);
        Button btn=(Button)findViewById(R.id.cmb);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int att=ms1.Attaque(ms2);

                TextView txt=(TextView) findViewById(R.id.pdv);
                TextView txt1=(TextView) findViewById(R.id.pdvadv);
                TextView txt2=(TextView) findViewById(R.id.Resultat);
                TextView txt3=(TextView) findViewById(R.id.Resultatadv);

                if(att>0 ){

                    txt2.setText("Attaque Reussi : "+att);
                    txt1.setText("VIE : "+ms2.getPointDeVie());
                }
                else{
                    txt2.setText("Attaque Echouer");
                }
                int att2=ms2.Attaque(ms1);
                if(att2>0){

                    txt3.setText("Attaque Reussi : "+att2);
                    txt.setText("VIE : "+ms1.getPointDeVie());

                }
                else{
                    txt3.setText("Attaque Echouer");
                }

                if(ms2.getPointDeVie()<=0){
                    AlertDialog.Builder adb= new AlertDialog.Builder(CombatNetwork.this);
                    adb.setTitle("Combat Terminer");
                    adb.setMessage("Monstre Gagnant "+ms1.getNom());
                    adb.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            finish();
                        }
                    });
                    adb.show();
                }
                else if(ms1.getPointDeVie()<=0){
                    AlertDialog.Builder adb= new AlertDialog.Builder(CombatNetwork.this);
                    adb.setTitle("Combat Terminer");
                    adb.setMessage("Monstre Gagnant "+ms2.getNom());
                    adb.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            finish();
                        }
                    });
                    adb.show();
                }

            }
        });
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CombatNetwork.this, SelectMonster.class);
                startActivityForResult(intent,2);

            }
        });
        img1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(CombatNetwork.this, Bluetooth.class);
                System.out.println("PPPPP"+ms1.getNom());
                intent.putExtra("MONSTERADVERSE",ms1);
                startActivityForResult(intent,1);

            }
        });


    }
}
