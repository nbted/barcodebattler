package com.example.deptinfo.barcodebattler;

/**
 * Created by deptinfo on 31/10/2017.
 */

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.example.deptinfo.barcodebattler.MyBluetoothService;

import java.io.IOException;
import java.util.HashMap;
import java.util.UUID;


public class AcceptThread extends Thread
{
    public static final UUID MY_UUID=new UUID(2,5);
    private static final String TAG = "debuging";
    private final BluetoothServerSocket mmServerSocket;
    private BluetoothAdapter bta;
    android.os.Handler handle;

    public AcceptThread(BluetoothAdapter bta , Handler handle)
    {
        // Use a temporary object that is later assigned to mmServerSocket
        // because mmServerSocket is final.
        this.handle = handle;
        BluetoothServerSocket tmp = null;
        try
        {
            // MY_UUID is the app's UUID string, also used by the client code.
            this.bta = bta;
            tmp = bta.listenUsingRfcommWithServiceRecord("MON_BT", MY_UUID);
        }
        catch (IOException e)
        {
            Log.e(TAG, "Socket's listen() method failed", e);
        }
        mmServerSocket = tmp;
    }

    public void run()
    {
        BluetoothSocket socket = null;
        // Keep listening until exception occurs or a socket is returned.
        while (true)
        {
            try
            {

                socket = mmServerSocket.accept();


            }
            catch (IOException e)
            {
                Log.e(TAG, "Socket's accept() method failed", e);
                break;
            }

            if (socket != null)
            {
                // A connection was accepted. Perform work associated with
                // the connection in a separate thread.
                manageMyConnectedSocket(socket,handle);

                try
                {
                    mmServerSocket.close();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    private void manageMyConnectedSocket(BluetoothSocket socket, android.os.Handler handle)
    {
        MyBluetoothService mbs = new MyBluetoothService(socket,handle);


    }

    // Closes the connect socket and causes the thread to finish.
    public void cancel()
    {
        try
        {
            mmServerSocket.close();
        }
        catch (IOException e)
        {
            Log.e(TAG, "Could not close the connect socket", e);
        }
    }
}